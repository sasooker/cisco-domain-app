import * as React from 'react';
import DomainList from '../domain/domain-list';

export default class DomainListPage extends React.PureComponent {
    render() {
        return (
            <DomainList />
        );
    }
}
