import * as React from 'react';
import DomainCreateForm from '../domain/domain-create-form';

export default class DomainCreatePage extends React.PureComponent {
    render() {
        return <DomainCreateForm />;
    }
}
