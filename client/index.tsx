import 'promise.prototype.finally';
import 'antd/dist/antd.css';
import './index.css';
import * as React from 'react';
import {Provider} from 'react-redux';
import ReactDOM from 'react-dom';
import Main from './main';
import store from './store';

ReactDOM.render(
    <Provider store={store}>
        <Main />
    </Provider>,
    document.getElementById('react-mount')
);
