import {combineReducers, createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {DomainState, domainReducer} from './domain/domain-reducer';

export type GlobalState = {
    domain: DomainState
};

const store = createStore(
    combineReducers({
        domain: domainReducer
    }),
    applyMiddleware(thunk)
);

export default store;
