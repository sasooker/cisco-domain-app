import * as React from 'react';
import {Router, Route, Switch, Redirect} from 'react-router';
import history from './history';
import DomainListPage from './pages/domain-list-page';
import DomainCreatePage from './pages/domain-create-page';
import DomainEditPage from './pages/domain-edit-page';

export default class Main extends React.PureComponent {
    render() {
        return (
            <Router history={history}>
                <Switch>
                    <Route exact path='/' component={DomainListPage} />
                    <Route path='/create' component={DomainCreatePage} />
                    <Route path='/edit/:id' component={DomainEditPage} />
                    <Route component={() => <Redirect to={'/'} />} />
                </Switch>
            </Router>
        );
    }
}
