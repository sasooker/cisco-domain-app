declare module 'domain-app' {
    interface Data<T> {
        data: T
    }

    interface DomainCommon {
        hostname: string;
        description: string | null;
        resolves: boolean;
    }

    interface Domain extends DomainCommon {
        id: number;
        createdAt: string;
        updatedAt: string;
    }

    interface DomainUpdate extends Partial<DomainCreate> {
        id: number;
    }

    interface DomainCreate {
        url: string;
        description: string;
    }

    type DomainError = DomainAttributeError | DomainGeneralError;

    interface DomainGeneralError {
        message: string;
    }

    interface DomainAttributeError {
        error: {
            [key: string]: string
        };
    }

    type VoidFn = (...args: any[]) => void;
}
