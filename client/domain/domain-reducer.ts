import {AxiosError} from 'axios';
import {Domain, DomainCreate, DomainUpdate} from 'domain-app';
import {DomainActions, ActionSymbols} from './domain-action';

export interface DomainState {
    domains: Domain[];
    updatedDomain: Domain | DomainCreate | DomainUpdate;
    error: {
        list?: AxiosError;
        create?: AxiosError;
        update?: AxiosError;
    };
    loading: boolean;
}

export const initialState: DomainState = {
    domains: [],
    loading: false,
    error: {},
    updatedDomain: {
        url: '',
        description: ''
    } as DomainCreate
};

export function domainReducer(state = initialState, action: DomainActions) {
    switch (action.type) {
        case ActionSymbols.REQUEST:
            return {...state, loading: true};
        case ActionSymbols.CLEAR_LOADING:
            return {...state, loading: false};
        case ActionSymbols.LIST_RESPONSE:
            return {...state, domains: action.payload};
        case ActionSymbols.UPDATE_PAYLOAD:
            return {...state, updatedDomain: {...state.updatedDomain, ...action.payload}};
        case ActionSymbols.LIST_ERROR:
            return {...state, error: {...state.error, list: action.payload}};
        case ActionSymbols.CREATE_ERROR:
            return {...state, error: {...state.error, create: action.payload}};
        case ActionSymbols.UPDATE_ERROR:
            return {...state, error: {...state.error, update: action.payload}};
        case ActionSymbols.CLEAR_ERROR:
            return {...state, error: {}};
        case ActionSymbols.RESET:
            return initialState;
    }

    return state;
}

