import {Domain, DomainCreate, DomainUpdate} from 'domain-app';
import {Dispatch} from 'redux';
import {AxiosError} from 'axios';
import history from '../history';
import {GlobalState} from '../store';
import * as DomainSource from './domain-source';

export enum ActionSymbols {
    REQUEST = 'DOMAIN_REQUEST',
    CLEAR_LOADING = 'DOMAIN_CLEAR_LOADING',
    CREATE_RESPONSE = 'DOMAIN_CREATE_RESPONSE',
    UPDATE_RESPONSE = 'DOMAIN_UPDATE_RESPONSE',
    LIST_RESPONSE = 'DOMAIN_LIST_RESPONSE',
    UPDATE_PAYLOAD = 'DOMAIN_UPDATE_PAYLOAD',
    RESET = 'DOMAIN_RESET',
    LIST_ERROR = 'DOMAIN_LIST_ERROR',
    UPDATE_ERROR = 'DOMAIN_UPDATE_ERROR',
    CREATE_ERROR = 'DOMAIN_CREATE_ERROR',
    CLEAR_ERROR = 'DOMAIN_CLEAR_ERROR',
}

export type DomainActions = DomainListRequest
    | DomainClearLoading
    | DomainCreateResponse
    | DomainUpdateResponse
    | DomainUpdatePayload
    | DomainListResponse
    | DomainReset
    | DomainListError
    | DomainCreateError
    | DomainUpdateError
    | DomainClearError;

export interface DomainListRequest {
    type: ActionSymbols.REQUEST;
}

export interface DomainListError {
    type: ActionSymbols.LIST_ERROR;
    payload: AxiosError;
}

export interface DomainClearLoading {
    type: ActionSymbols.CLEAR_LOADING;
}

export interface DomainCreateResponse {
    type: ActionSymbols.CREATE_RESPONSE;
    payload: Domain;
}

export interface DomainCreateError {
    type: ActionSymbols.CREATE_ERROR;
    payload: AxiosError;
}

export interface DomainUpdateError {
    type: ActionSymbols.UPDATE_ERROR;
    payload: AxiosError;
}

export interface DomainUpdateResponse {
    type: ActionSymbols.UPDATE_RESPONSE;
    payload: Domain;
}

export interface DomainUpdatePayload {
    type: ActionSymbols.UPDATE_PAYLOAD;
    payload: Partial<Domain>;
}

export interface DomainListResponse {
    type: ActionSymbols.LIST_RESPONSE;
    payload: Domain[];
}

export interface DomainClearError {
    type: ActionSymbols.CLEAR_ERROR;
}

export interface DomainReset {
    type: ActionSymbols.RESET;
}

export function query() {
    return (dispatch: Dispatch<Domain[]>) => {
        dispatch(request());

        return DomainSource.query()
            .then((response) => dispatch(listResponse(response.data)))
            .catch((e) => dispatch(listError(e)))
            .finally(() => dispatch(clearLoading()));
    };
}

export function create() {
    return (dispatch: Dispatch<Domain>, getState: () => GlobalState) => {
        dispatch(request());

        return DomainSource.create(getState().domain.updatedDomain as DomainCreate)
            .then(() => history.push('/'))
            .catch((e) => dispatch(createError(e)))
            .finally(() => dispatch(clearLoading()));
    }
}

export function update(domain: DomainUpdate) {
    return (dispatch: Dispatch<Domain>) => {
        dispatch(request());

        return DomainSource.update(domain)
            .catch((e) => dispatch(updateError(e)))
            .finally(() => dispatch(clearLoading()));
    }
}

export function remove(id: number) {
    return (dispatch: Dispatch<void>) => {
        return DomainSource.remove(id)
            .then(() => dispatch(query()))
            .catch((e) => dispatch(listError(e)));
    }
}

export function listError(payload: AxiosError): DomainListError {
    return {
        type: ActionSymbols.LIST_ERROR,
        payload
    };
}

export function createError(payload: AxiosError): DomainCreateError {
    return {
        type: ActionSymbols.CREATE_ERROR,
        payload
    };
}

export function updateError(payload: AxiosError): DomainUpdateError {
    return {
        type: ActionSymbols.UPDATE_ERROR,
        payload
    };
}

export function request(): DomainListRequest {
    return {
        type: ActionSymbols.REQUEST
    };
}

export function clearLoading(): DomainClearLoading {
    return {
        type: ActionSymbols.CLEAR_LOADING
    };
}

export function createResponse(payload: Domain): DomainCreateResponse {
    return {
        type: ActionSymbols.CREATE_RESPONSE,
        payload
    };
}

export function updateResponse(payload: Domain): DomainUpdateResponse {
    return {
        type: ActionSymbols.UPDATE_RESPONSE,
        payload
    };
}

export function updatePayload(payload: DomainUpdate): DomainUpdatePayload {
    return {
        type: ActionSymbols.UPDATE_PAYLOAD,
        payload
    };
}

export function listResponse(payload: Domain[]): DomainListResponse {
    return {
        type: ActionSymbols.LIST_RESPONSE,
        payload
    };
}

export function clearError(): DomainClearError {
    return {
        type: ActionSymbols.CLEAR_ERROR
    };
}
