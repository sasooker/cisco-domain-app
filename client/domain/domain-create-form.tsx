import * as React from 'react';
import {DomainCreate, VoidFn} from 'domain-app';
import {Dispatch, bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {AxiosError} from 'axios';
import {clearError, create, updatePayload} from '../domain/domain-action';
import {GlobalState} from '../store';
import {Alert, Form, Input, Button} from 'antd';

interface DomainCreateFormProps {
    domain: DomainCreate;
    loading: boolean;
    error: AxiosError;
    update(domain: Partial<DomainCreate>): void;
    save(): void;
    clearError(): void;
}

class DomainCreateForm extends React.PureComponent<DomainCreateFormProps> {
    componentWillUnmount() {
        this.props.clearError();
    }

    save = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        this.props.save();
    }

    updateUrl = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.update({url: e.target.value});
    }

    updateDescription = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        this.props.update({description: e.target.value});
    }

    render() {
        return (
            <>
                {this.props.error && this.props.error.response
                    ? <Alert message={
                            // bleh
                          this.props.error.response.data.message
                            || `this host ${this.props.error.response.data.errors.hostname[0]}`
                      } type='error' showIcon />
                    : null
                }

                <Form
                    onSubmit={this.save}
                >
                    <Form.Item label='URL'>
                        <Input
                            type='text'
                            placeholder='Enter full URL'
                            onChange={this.updateUrl}
                        />
                    </Form.Item>
                    <Form.Item label='Description'>
                        <Input.TextArea
                            placeholder='Enter a description'
                            onChange={this.updateDescription}
                        />
                    </Form.Item>

                    <Button htmlType='submit' type='primary' loading={this.props.loading}>Create!</Button>
                </Form>
            </>
        );
    }
}

function mapStateToProps(state: GlobalState) {
    return {
        domain: state.domain.updatedDomain,
        loading: state.domain.loading,
        error: state.domain.error.create
    };
}

function mapDispatchToProps(dispatch: Dispatch<GlobalState>) {
    return bindActionCreators({
        update: updatePayload as VoidFn,
        save: create as VoidFn,
        clearError: clearError as VoidFn
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(DomainCreateForm);
export {DomainCreateForm};
