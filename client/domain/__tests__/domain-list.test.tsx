import * as React from 'react';
import {shallow} from 'enzyme';
import {DomainList} from '../domain-list';

test('it renders correctly', () => {
    const tree = shallow(
        <DomainList
            domains={[
                {
                    id: 1,
                    hostname: 'host',
                    description: '',
                    resolves: true,
                    createdAt: 'now',
                    updatedAt: 'now'
                }, {
                    id: 2,
                    hostname: 'hosty',
                    description: '',
                    resolves: false,
                    createdAt: 'now',
                    updatedAt: 'now'
                }
            ]}
            loading={false}
            query={() => {}}
            removeDomain={() => {}}
        />
    )

    expect(tree).toMatchSnapshot();
});
