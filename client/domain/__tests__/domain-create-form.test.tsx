import * as React from 'react';
import {shallow} from 'enzyme';
import {DomainCreateForm} from '../domain-create-form';

test('it renders correctly', () => {
    const tree = shallow(
        <DomainCreateForm
            domain={{
                url: 'url',
                description: ''
            }}
            error={undefined as any}
            loading={false}
            update={() => {}}
            save={() => {}}
            clearError={() => {}}
        />
    )

    expect(tree).toMatchSnapshot();
});
