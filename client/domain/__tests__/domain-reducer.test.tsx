import {initialState, domainReducer} from '../domain-reducer';
import {
    request,
    clearLoading,
    clearError,
    listResponse,
    updatePayload,
    listError,
    createError,
    updateError
} from '../domain-action';

describe('domainReducer', () => {
    test('REQUEST', () => {
        expect(
            domainReducer({...initialState, loading: false}, request())
        ).toEqual({...initialState, loading: true});
    });

    test('CLEAR_LOADING', () => {
        expect(
            domainReducer({...initialState, loading: true}, clearLoading())
        ).toEqual({...initialState, loading: false});
    });

    test('CLEAR_ERROR', () => {
        expect(
            domainReducer({...initialState, error: {update: {} as any}}, clearError())
        ).toEqual({...initialState, error: {}});
    });

    test('LIST_RESPONSE', () => {
        expect(
            domainReducer(initialState, listResponse([{} as any]))
        ).toEqual({...initialState, domains: [{}]});
    });

    test('UPDATE_PAYLOAD', () => {
        expect(
            domainReducer(initialState, updatePayload({url: 'hello'} as any))
        ).toEqual({...initialState, updatedDomain: {url: 'hello', description: ''}});
    });

    test('LIST_ERROR', () => {
        expect(
            domainReducer(initialState, listError({} as any))
        ).toEqual({...initialState, error: {list: {}}});
    });

    test('CREATE_ERROR', () => {
        expect(
            domainReducer(initialState, createError({} as any))
        ).toEqual({...initialState, error: {create: {}}});
    });

    test('UPDATE_ERROR', () => {
        expect(
            domainReducer(initialState, updateError({} as any))
        ).toEqual({...initialState, error: {update: {}}});
    });
});
