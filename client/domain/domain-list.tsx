import * as React from 'react';
import {Domain, VoidFn} from 'domain-app';
import {Dispatch, bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {Row, Col, Table, Icon, Button, Popconfirm} from 'antd';
import {GlobalState} from '../store';
import * as DomainActions from '../domain/domain-action';

interface DomainListProps {
    domains: Domain[];
    loading: boolean;
    query(): void;
    removeDomain(id: number): void;
}

class DomainList extends React.PureComponent<DomainListProps> {
    componentDidMount() {
        this.props.query()
    }

    render() {
        return (
            <>
                <Row>
                    <Col span={4}>
                        <Link to='/create'>
                            <Button type='primary'>Add new DNS entry</Button>
                        </Link>
                    </Col>
                    <Col span={4} offset={16}>
                        <Button
                            onClick={this.props.query}
                            style={{float: 'right'}}
                        >
                            Reload
                        </Button>
                    </Col>
                </Row>

                <Table
                    dataSource={this.props.domains}
                    loading={this.props.loading}
                    rowKey='id'
                >
                    <Table.Column
                        title='Host'
                        dataIndex='hostname'
                        key='hostname'
                    />

                    <Table.Column
                        title='Description'
                        dataIndex='description'
                        key='description'
                    />

                    <Table.Column
                        title='DNS Resolves'
                        dataIndex='resolves'
                        key='resolves'
                        render={(text: boolean) =>
                            <Icon type={text ? 'check' : 'close'} />
                        }
                    />

                    <Table.Column
                        title='Created At'
                        dataIndex='createdAt'
                        key='createdAt'
                    />

                    <Table.Column
                        title='Delete'
                        key='delete'
                        render={(_, record: Domain) =>
                            <Popconfirm
                                title='Are you sure you want to delete this record?'
                                onConfirm={() => this.props.removeDomain(record.id)}
                            >
                                <Button type='danger'>
                                    Delete
                                </Button>
                            </Popconfirm>
                        }
                    />
                </Table>
            </>
        );
    }
}

function mapStateToProps(state: GlobalState) {
    return {
        domains: state.domain.domains,
        loading: state.domain.loading
    };
}

function mapDispatchToProps(dispatch: Dispatch<GlobalState>) {
    return bindActionCreators({
        query: DomainActions.query as VoidFn,
        removeDomain: DomainActions.remove as VoidFn
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(DomainList);
export {DomainList};
