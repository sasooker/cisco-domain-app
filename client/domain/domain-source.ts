import axios, {AxiosResponse} from 'axios';
import {Data, Domain, DomainCreate, DomainUpdate} from 'domain-app';

export function query(): Promise<Data<Domain[]>> {
    return axios.get('/api/domains')
        .then((response) => response.data);
}

export function fetch(id: number): Promise<Data<Domain>> {
    return axios.get(`/api/domains/${id}`)
        .then((response) => response.data);
}

export function create(domain: DomainCreate): Promise<Data<Domain>> {
    return axios.post('/api/domains', {domain})
        .then((response) => response.data);
}

export function update(domain: DomainUpdate): Promise<Data<Domain>> {
    return axios.put(`/api/domains/${domain.id}`, {domain})
        .then((response) => response.data);
}

export function remove(id: number): Promise<AxiosResponse<void>> {
    return axios.delete(`/api/domains/${id}`);
}
