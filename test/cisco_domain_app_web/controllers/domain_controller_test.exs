defmodule CiscoDomainAppWeb.DomainControllerTest do
  import Mock
  use CiscoDomainAppWeb.ConnCase

  alias CiscoDomainApp.Domains
  alias CiscoDomainApp.Domains.Domain

  @create_attrs %{"url" => "http://subdomain.hostyhost.abc", "description" => "some description"}
  @update_attrs %{"description" => "some updated description"}
  @invalid_hostname %{"url" => "hostyhosty"}
  @invalid_tld %{"url" => "http://hostyhost"}

  def fixture(:domain) do
    {:ok, domain} = Domains.create_domain(@create_attrs)
    domain
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  setup_with_mocks([
    {DNS, [], [resolve: fn(_url) -> {:ok, []} end]}
  ]) do
    :ok
  end

  describe "index" do
    test "lists all domains", %{conn: conn} do
      conn = get conn, domain_path(conn, :index)
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create domain" do
    test "renders domain when data is valid", %{conn: conn} do
      conn = post conn, domain_path(conn, :create), domain: @create_attrs
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get conn, domain_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "resolves" => true,
        "hostname" => "hostyhost.abc",
        "description" => "some description"}
    end

    test "renders errors when does not have a parseable hostname", %{conn: conn} do
      conn = post conn, domain_path(conn, :create), domain: @invalid_hostname
      assert %{"message" => message} = json_response(conn, 422)
      assert Regex.match? ~r/host/i, message
    end

    test "renders errors when invalid tld", %{conn: conn} do
      conn = post conn, domain_path(conn, :create), domain: @invalid_tld
      assert %{"message" => message} = json_response(conn, 422)
      assert Regex.match? ~r/domain/i, message
    end

    test_with_mock "renders domain with resolves set to false if DNS does not resolve", %{conn: conn}, DNS, [],
        [resolve: fn(_url) -> {:error, :not_found} end] do

      conn = post conn, domain_path(conn, :create), domain: @create_attrs
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get conn, domain_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "resolves" => false,
        "hostname" => "hostyhost.abc",
        "description" => "some description"}
    end
  end

  describe "update domain" do
    setup [:create_domain]

    test "renders domain when data is valid", %{conn: conn, domain: %Domain{id: id} = domain} do
      conn = put conn, domain_path(conn, :update, domain), domain: @update_attrs
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get conn, domain_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "resolves" => true,
        "hostname" => "hostyhost.abc",
        "description" => "some updated description"}
    end

    test "renders errors when data is invalid", %{conn: conn, domain: domain} do
      conn = put conn, domain_path(conn, :update, domain), domain: @invalid_hostname
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete domain" do
    setup [:create_domain]

    test "deletes chosen domain", %{conn: conn, domain: domain} do
      conn = delete conn, domain_path(conn, :delete, domain.id)
      assert response(conn, 204)
      assert_error_sent 404, fn ->
        get conn, domain_path(conn, :show, domain)
      end
    end
  end

  defp create_domain(_) do
    domain = fixture(:domain)
    {:ok, domain: domain}
  end
end
