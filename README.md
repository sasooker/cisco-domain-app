# Cisco Domain App

## Getting Started

1. Download [docker](https://www.docker.com/) and [docker-compose](https://docs.docker.com/compose/install/).
2. Run `docker-compose build`
3. Run `docker-compose up`
    * The database will seed from [this file](https://bitbucket.org/sasooker/cisco-domain-app/src/361745f4b29162f049be681df23e4d89d79ca502/priv/repo/init/init.sql)
4. By default, you can access the webapp at `http://localhost:4000`
5. Hitting [here](http://localhost:4000) should provide you with a list of the DNS entries.
6. Hitting [here](http://localhost:4000/create) should result take you to the entry creation form.
7. The [api](http://localhost:4000/api/domains) has the standard REST verbs (`GET`, `POST`, `PUT`, `DELETE`) available.

## Running Tests
1. Directly from the application
    * `mix test`
    * `npm test`
2. You can execute it through Docker
    * `docker-compose exec web mix test`
    * `docker-compose exec web npm test`

## API

| Endpoint       | Verb | Action |
| ---------------|------|--------|
| `/api/domains` | `GET`| Retrieves a list of all the domains ordered by `createdAt` descending |
| `/api/domains/:id` | `GET` | Retrieves a single domain |
| `/api/domains` | `POST` | Creates a new domain |
| `/api/domains/:id` | `PUT` | Updates an existing domain |
| `/api/domains/:id` | `DELETE` | Deletes an existing domain if it exists |

## File Layout
* You can find the requisite `sql` file at `/priv/repo/init/init.sql`; Docker also calls this on first run.
* `/lib/cisco_domain_app/domains/domains.ex:116` has a great example of Elixir's pattern-matching powers. Super cool!

## Technical Choices

> Also, domains should be stored in the database without any "www" prefix and without any other URL components. For example, "https:/www.opendns.com/about" should be stored as "opendns.com".

I took this to mean that I should strip subdomains from the provided URLs (as www is a valid subdomain). Hopefully that was correct!  Right now the application will strip the subdomain and verify that there's a DNS entry in Google's `8.8.8.8` DNS server. If so, it gets marked as `resolves`.

1. Used [elixir](https://elixir-lang.org/) for the backend, as it was a personal interest of mine.
2. Opted to create a Single Page Application, as React is quite good for it.
3. Used the following front-end technologies:
    1. React
    2. Redux
    3. Redux-thunk
    4. React-router
    5. Webpack
    6. Axios - Wanted the built-in handling of rejecting non-2xx/3xx responses. I've overwritten the default `fetch` behaviors enough times.
    7. [Ant design](https://ant.design/) - A nice UI library that I was interested in trying out. I must say, I liked it a lot.
    8. Jest - I like the snapshot testing.
4. I did not end up writing much CSS, as Ant had most of the things I wanted.

