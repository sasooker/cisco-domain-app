use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :cisco_domain_app, CiscoDomainAppWeb.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :cisco_domain_app, CiscoDomainApp.Repo,
  adapter: Ecto.Adapters.MySQL,
  username: "domain_test",
  password: "tester",
  database: "cisco_domain_app_test",
  hostname: System.get_env("DB_HOST"),
  pool: Ecto.Adapters.SQL.Sandbox
