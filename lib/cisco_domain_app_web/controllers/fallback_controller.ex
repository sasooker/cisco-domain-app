defmodule CiscoDomainAppWeb.FallbackController do
  @moduledoc """
  Translates controller action results into valid `Plug.Conn` responses.

  See `Phoenix.Controller.action_fallback/1` for more details.
  """
  use CiscoDomainAppWeb, :controller

  def call(conn, {:error, %Ecto.Changeset{} = changeset}) do
    conn
    |> put_status(:unprocessable_entity)
    |> render(CiscoDomainAppWeb.ChangesetView, "error.json", changeset: changeset)
  end

  def call(conn, {:error, :not_found}) do
    conn
    |> put_status(:not_found)
    |> render(CiscoDomainAppWeb.ErrorView, :"404")
  end

  def call(conn, {:error, :bad_request}) do
    conn
    |> put_status(:bad_request)
    |> render(CiscoDomainAppWeb.ErrorView, :"400")
  end

  def call(conn, {:error, message}) when is_binary(message) do
    json conn |> put_status(:unprocessable_entity), %{message: message}
  end
end
