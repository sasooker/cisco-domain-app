defmodule CiscoDomainAppWeb.DomainController do
  use CiscoDomainAppWeb, :controller

  alias CiscoDomainApp.Domains
  alias CiscoDomainApp.Domains.Domain

  action_fallback CiscoDomainAppWeb.FallbackController

  def index(conn, _params) do
    domains = Domains.list_domains()
    render(conn, "index.json", domains: domains)
  end

  def create(conn, %{"domain" => domain_params}) do
    with {:ok, %Domain{} = domain} <- Domains.create_domain(domain_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", domain_path(conn, :show, domain))
      |> render("show.json", domain: domain)
    end
  end

  def show(conn, %{"id" => id}) do
    domain = Domains.get_domain!(id)
    render(conn, "show.json", domain: domain)
  end

  def update(conn, %{"id" => id, "domain" => domain_params}) do
    domain = Domains.get_domain!(id)

    with {:ok, %Domain{} = domain} <- Domains.update_domain(domain, domain_params) do
      render(conn, "show.json", domain: domain)
    end
  end

  def delete(conn, %{"id" => id}) do
    domain = Domains.get_domain!(id)
    with {:ok, %Domain{}} <- Domains.delete_domain(domain) do
      send_resp(conn, :no_content, "")
    end
  end
end
