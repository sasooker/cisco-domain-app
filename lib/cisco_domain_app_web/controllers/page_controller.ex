defmodule CiscoDomainAppWeb.PageController do
  use CiscoDomainAppWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
