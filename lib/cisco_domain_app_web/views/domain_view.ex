defmodule CiscoDomainAppWeb.DomainView do
  use CiscoDomainAppWeb, :view
  alias CiscoDomainAppWeb.DomainView

  def render("index.json", %{domains: domains}) do
    %{data: render_many(domains, DomainView, "domain.json")}
  end

  def render("show.json", %{domain: domain}) do
    %{data: render_one(domain, DomainView, "domain.json")}
  end

  def render("domain.json", %{domain: domain}) do
    %{id: domain.id,
      description: domain.description,
      hostname: domain.hostname,
      resolves: domain.resolves,
      createdAt: domain.inserted_at,
      updatedAt: domain.updated_at}
  end
end
