defmodule CiscoDomainApp.Domains.Domain do
  use Ecto.Schema
  import Ecto.Changeset


  schema "domains" do
    field :hostname, :string
    field :description, :string
    field :resolves, :boolean

    timestamps()
  end

  @doc false
  def changeset(domain, attrs) do
    domain
    |> cast(attrs, [:description, :hostname, :resolves])
    |> validate_required([:hostname])
    |> unique_constraint(:hostname, name: :UX__DOMAIN__HOSTNAME)
  end
end
