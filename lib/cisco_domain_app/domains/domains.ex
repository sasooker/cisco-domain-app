defmodule CiscoDomainApp.Domains do
  @moduledoc """
  The Domains context.
  """

  import Ecto.Query, warn: false
  alias CiscoDomainApp.Repo

  alias CiscoDomainApp.Domains.Domain

  @doc """
  Returns the list of domains.

  ## Examples

      iex> list_domains()
      [%Domain{}, ...]

  """
  def list_domains do
    Domain
    |> order_by(desc: :inserted_at)
    |> Repo.all()
  end

  @doc """
  Gets a single domain.

  Raises `Ecto.NoResultsError` if the Domain does not exist.

  ## Examples

      iex> get_domain!(123)
      %Domain{}

      iex> get_domain!(456)
      ** (Ecto.NoResultsError)

  """
  def get_domain!(id), do: Repo.get!(Domain, id)

  @doc """
  Creates a domain.

  ## Examples

      iex> create_domain(%{field: value})
      {:ok, %Domain{}}

      iex> create_domain(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_domain(attrs \\ %{}) do
    with {:ok, command} <- validate_domain(attrs) do
      %Domain{}
      |> Domain.changeset(command)
      |> Repo.insert()
    end
  end

  @doc """
  Updates a domain.

  ## Examples

      iex> update_domain(domain, %{field: new_value})
      {:ok, %Domain{}}

      iex> update_domain(domain, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_domain(%Domain{} = domain, attrs) do
    with {:ok, command} <- validate_domain(attrs) do
      domain
      |> Domain.changeset(command)
      |> Repo.update()
    end
  end

  @doc """
  Deletes a Domain.

  ## Examples

      iex> delete_domain(domain)
      {:ok, %Domain{}}

      iex> delete_domain(domain)
      {:error, %Ecto.Changeset{}}

  """
  def delete_domain(%Domain{} = domain) do
    Repo.delete(domain)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking domain changes.

  ## Examples

      iex> change_domain(domain)
      %Ecto.Changeset{source: %Domain{}}

  """
  def change_domain(%Domain{} = domain) do
    Domain.changeset(domain, %{})
  end

  @doc """
  Verifies that a provided URL is a valid domain

  Also checks DNS to see if there is a valid entry as well.
  """
  defp validate_domain(%{"url" => url, "description" => description}) when url != nil do
    with %URI{host: hostname} when hostname != nil <- URI.parse(url),
         {:ok, %{domain: domain, tld: tld}} <- Domainatrex.parse(hostname),
         domain_and_tld <- "#{domain}.#{tld}",
         {resolved, _resolved_ips} <- DNS.resolve(domain_and_tld) do

      {:ok, %{hostname: domain_and_tld,
              resolves: resolved == :ok,
              description: description}}

    else
      %{host: nil} -> {:error, "unable to parse host"}
      {:error, "Cannot parse: invalid domain"} -> {:error, "invalid domain provided"}
    end
  end

  defp validate_domain(%{"url" => url}) do
    validate_domain(%{"url" => url, "description" => ""})
  end

  defp validate_domain(%{"description" => description}) do
    {:ok, %{description: description}}
  end

  defp validate_domain(%{"url" => nil, "description" => description}) do
    {:ok, %{description: description}}
  end
end
