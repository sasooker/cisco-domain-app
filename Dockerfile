FROM bitwalker/alpine-elixir-phoenix:1.6.4

# Using dockerize to tell when mysql is ready

RUN apk add --no-cache openssl

ENV DOCKERIZE_VERSION v0.6.1
RUN wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz

EXPOSE 5000
ENV PORT=5000
ENV MIX_ENV=dev
ENV NODE_ENV=development

ADD . .

RUN mix do deps.get, deps.compile

RUN npm install -g yarn
RUN yarn install
RUN yarn run build
