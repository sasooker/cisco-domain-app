const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const webpack = require('webpack');

const STATIC_PATH = path.resolve(__dirname, 'priv', 'static');

module.exports = {
    target: 'web',
    entry: './client/index',
    output: {
        path: path.join(STATIC_PATH, 'js'),
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: 'css-loader'
                })
            },
            {
                test: /\.(j|t)sx?$/,
                exclude: [/node_modules/],
                loader: 'ts-loader'
            }
        ]
    },
    resolve: {
        extensions: ['.js', '.jsx', '.ts', '.tsx']
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            filename: 'common.js'
        }),
        new ExtractTextPlugin({
            filename: '../css/app.css'
        })
    ]
}
