CREATE USER 'domain'@'%' IDENTIFIED BY 'tester';
CREATE DATABASE cisco_domain_app_dev;
GRANT ALL PRIVILEGES ON cisco_domain_app_dev.* TO domain;

CREATE USER 'domain_test'@'%' IDENTIFIED BY 'tester';
CREATE DATABASE cisco_domain_app_test;
GRANT ALL PRIVILEGES ON cisco_domain_app_test.* TO domain_test;

CREATE TABLE cisco_domain_app_dev.domains (
    id BIGINT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    hostname VARCHAR(2048) NOT NULL,
    description TEXT,
    resolves BOOLEAN NOT NULL,
    inserted_at DATETIME NOT NULL,
    updated_at DATETIME NOT NULL,
    CONSTRAINT UX__DOMAIN__HOSTNAME UNIQUE (hostname)
);

CREATE TABLE cisco_domain_app_test.domains (
    id BIGINT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    hostname VARCHAR(2048) NOT NULL,
    description TEXT,
    resolves BOOLEAN NOT NULL,
    inserted_at DATETIME NOT NULL,
    updated_at DATETIME NOT NULL,
    CONSTRAINT UX__DOMAIN__HOSTNAME UNIQUE (hostname)
);
